import React from 'react'
import ReactDOM from 'react-dom'
import Container from './components/Container.jsx'

const root = document.getElementById('root')
root ? ReactDOM.render(<Container/>, root) : false
