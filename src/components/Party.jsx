import React from 'react'

const Party = (props) => {
  const party = props.party
  return <span>
    {party.bank_code} / {party.branch_code} / {party.account_number} / {party.reference_code} / {party.name}
  </span>
}

export default Party
