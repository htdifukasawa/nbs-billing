import React, { Component } from 'react'

const options = [
  {value: '2019-01', label: '2019 January'},
  {value: '2018-12', label: '2018 December'},
  {value: '2018-11', label: '2018 November'},
  {value: '2018-10', label: '2018 October'},
  {value: '2018-09', label: '2018 September'},
]

class SMBC extends Component {
  constructor(props) {
    super()
    this.state = {
      selectedBeneficiary: props.beneficiary,
      selectedMonth: options[0].value
    }
    this.monthChanged = this.monthChanged.bind(this)
    this.downloadSMBC = this.downloadSMBC.bind(this)
  }

  monthChanged(event) {
    this.setState({selectedMonth: event.target.value})
  }

  downloadSMBC() {
    const link = document.createElement('a')
    link.setAttribute('href', `/smbc/beneficiary/${this.state.selectedBeneficiary.id}/${this.state.selectedMonth.split('-')[0]}/${this.state.selectedMonth.split('-')[1]}`)
    link.setAttribute('download', `beneficiary-smbc-${this.state.selectedBeneficiary.id}.txt`)
    document.body.appendChild(link)
    link.click()
    document.body.removeChild(link)
  }

  render() {
    return (
      <div className="input-group">
        <select className="custom-select" onChange={this.monthChanged}>
          {options.map((entry) => {
            return <option key={entry.value} value={entry.value}>{entry.label}</option>
          })}
        </select>
        <div className="input-group-append">
          <button className="btn btn-outline-primary" type="button" onClick={this.downloadSMBC}>SMBC</button>
        </div>
      </div>
    )
  }
}

export default SMBC
