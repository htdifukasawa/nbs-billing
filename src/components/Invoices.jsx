import React from 'react'

import Party from './Party.jsx'

const isPaid = (invoice) => {
  return !!invoice.paid_date
}

const isOverdue = (invoice) => {
  return !isPaid(invoice) && Date.parse(invoice.due_date) < Date.now()
}

const Invoices = (props) => (
  <table className="table table-hover">
    <thead className="thead-dark">
    <tr>
      <th>Date</th>
      <th>Closing date</th>
      <th>Due date</th>
      <th className="text-right">Amount</th>
      <th>Reference code</th>
      <th>Remitter</th>
      <th>Paid date</th>
    </tr>
    </thead>
    <tbody>
    {props.invoices.map(i => {
      return <tr key={i.id}
                 className={`${isPaid(i) ? 'table-success' : ''} ${isOverdue(i) ? 'table-danger' : ''}`}>
        <td>{i.date}</td>
        <td>{i.closing_date}</td>
        <td>{i.due_date}</td>
        <td className="text-right">{i.amount}</td>
        <td>{i.reference_code}</td>
        <td><Party party={i.remitter}/></td>
        <td>{i.paid_date}</td>
      </tr>
    })}
    </tbody>
  </table>
)

export default Invoices
