import React, { Component } from 'react'

import Party from './Party.jsx'
import Invoices from './Invoices.jsx'
import Pagination from './Pagination.jsx'
import SMBC from './SMBC.jsx'

class Beneficiaries extends Component {
  constructor() {
    super()
    this.state = {
      invoices: undefined,
      beneficiaries: [],
      selectedBeneficiaryId: null
    }
    this.selectBeneficiary = this.selectBeneficiary.bind(this)
    this.selectInvoicesPage = this.selectInvoicesPage.bind(this)
  }

  selectBeneficiary(id) {
    fetch(`/api/beneficiary/${id}/invoices`).then(results => {
      return results.json()
    }).then(data => {
      this.setState({
        invoices: data,
        selectedBeneficiaryId: id
      })
    })
  }

  componentDidMount() {
    fetch('/api/beneficiary?results_per_page=100').then(results => {
      return results.json()
    }).then(data => {
      this.setState({
        beneficiaries: data.objects,
      })
      this.selectBeneficiary(data.objects[0].id)
    })
  }

  selectInvoicesPage(page) {
    fetch(`/api/beneficiary/${this.state.selectedBeneficiaryId}/invoices?page=${page}`).then(results => {
      return results.json()
    }).then(data => {
      this.setState({
        invoices: data
      })
    })
  }

  render() {
    let invoices, pagination = ''
    if (this.state.invoices !== undefined) {
      invoices = <Invoices invoices={this.state.invoices.objects}/>
      pagination = <Pagination total={this.state.invoices.total_pages}
                               current={this.state.invoices.page}
                               onChange={this.selectInvoicesPage}/>
    }

    return (
      <div className="card">
        <div className="card-header">
          <ul className="nav nav-pills card-header-pills" role="tablist">
            {this.state.beneficiaries.map(b => {
              return <li key={b.id} className="nav-item">
                <button className={`btn btn-link nav-link ${this.state.selectedBeneficiaryId === b.id ? 'active' : ''}`}
                        onClick={() => this.selectBeneficiary(b.id)}
                        data-toggle="tab" role="tab">{b.name}</button>
              </li>
            })}
          </ul>
        </div>
        <div className="tab-content">
          {this.state.beneficiaries.map(b => {
            return <div key={b.id}
                        className={`tab-pane ${this.state.selectedBeneficiaryId === b.id ? 'active' : ''}`}
                        role="tabpanel">
              <div className="form-row p-3">
                <div className="col-6">
                  <span className="form-control">
                    <Party party={b}/>
                  </span>
                </div>
                <div className="col-6">
                  <SMBC beneficiary={b}/>
                </div>
              </div>
            </div>
          })}
          {invoices}
          {pagination}
        </div>
      </div>
    )
  }
}

export default Beneficiaries
