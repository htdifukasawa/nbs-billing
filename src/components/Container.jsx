import React from 'react'
import Beneficiaries from './Beneficiaries.jsx'

const Container = () => (
  <div className="container-fluid pt-3">
    <Beneficiaries/>
  </div>
)

export default Container
