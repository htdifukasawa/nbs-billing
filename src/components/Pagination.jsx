import React from 'react'

const LEFT_PAGE = 'LEFT'
const RIGHT_PAGE = 'RIGHT'

const NEIGHBOURS = 7


const range = (from, to) => {
  let i = from
  const result = []

  while (i <= to) {
    result.push(i++)
  }

  return result
}

const pages = (total, current) => {

  const totalNumbers = (NEIGHBOURS * 2) + 3

  if (total <= totalNumbers + 2) {
    return range(1, total)
  }

  const startPage = Math.max(2, current - NEIGHBOURS)
  const endPage = Math.min(total - 1, current + NEIGHBOURS)

  let pages = range(startPage, endPage)

  const hasLeftSpill = startPage > 2
  const hasRightSpill = (total - endPage) > 1
  const spillOffset = totalNumbers - (pages.length + 1)

  if (hasLeftSpill && !hasRightSpill) {
    pages = [LEFT_PAGE, ...range(startPage - spillOffset, startPage - 1), ...pages]
  } else if (!hasLeftSpill && hasRightSpill) {
    pages = [...pages, ...range(endPage + 1, endPage + spillOffset), RIGHT_PAGE]
  } else {
    pages = [LEFT_PAGE, ...pages, RIGHT_PAGE]
  }

  return [1, ...pages, total]
}

const moveLeft = (props) => {
  props.onChange(Math.max(1, props.current - (NEIGHBOURS * 2) - 1))
}

const moveRight = (props) => {
  props.onChange(Math.min(props.total, props.current + (NEIGHBOURS * 2) + 1))
}

const Pagination = (props) => (
  <nav>
    <ul className="pagination pagination-sm justify-content-center">
      {pages(props.total, props.current).map((page) => {
        if (page === LEFT_PAGE) {
          return <li key={page} className="page-item">
            <span className="page-link"
                  onClick={() => moveLeft(props)}>&laquo;</span>
          </li>
        } else if (page === RIGHT_PAGE) {
          return <li key={page} className="page-item">
            <span className="page-link"
                  onClick={() => moveRight(props)}>&raquo;</span>
          </li>
        } else if (page === props.current) {
          return <li key={page} className="page-item active">
            <span className="page-link">{page}</span>
          </li>
        } else {
          return <li key={page} className="page-item">
            <span className="page-link"
                  onClick={() => props.onChange(page)}>{page}</span>
          </li>
        }
      })}
    </ul>
  </nav>
)

export default Pagination
