from sqlalchemy import *
from migrate.changeset.constraint import ForeignKeyConstraint

meta = MetaData()

remitter = Table(
    'remitter', meta,
    Column('id', Integer, primary_key=True),
    Column('name', String(255), nullable=False),
    Column('reference_code', String(40), nullable=False),
    Column('bank_code', String(10), nullable=False),
    Column('branch_code', String(10), nullable=False),
    Column('account_number', String(40), nullable=False),
)

remitter_id = Column('remitter_id', Integer,
                     ForeignKey(remitter.c.id, name='invoice_remitter_id_fk'), nullable=False)

invoice = Table(
    'invoice', meta,
    remitter_id,
)


def upgrade(migrate_engine):
    meta.bind = migrate_engine
    remitter.create()
    remitter_id.create(invoice)


def downgrade(migrate_engine):
    meta.bind = migrate_engine
    ForeignKeyConstraint(table=invoice, name='invoice_remitter_id_fk',
                         columns=[invoice.c.remitter_id], refcolumns=[remitter.c.id]).drop()
    remitter_id.drop(invoice)
    remitter.drop()
