from sqlalchemy import *

meta = MetaData()

customer = Table(
    'customer', meta,
    Column('id', Integer, primary_key=True),
    Column('code', String(40), nullable=False, unique=True),
)


def upgrade(migrate_engine):
    meta.bind = migrate_engine
    customer.create()


def downgrade(migrate_engine):
    meta.bind = migrate_engine
    customer.drop()
