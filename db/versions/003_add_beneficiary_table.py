from sqlalchemy import *
from migrate.changeset.constraint import ForeignKeyConstraint

meta = MetaData()

beneficiary = Table(
    'beneficiary', meta,
    Column('id', Integer, primary_key=True),
    Column('name', String(255), nullable=False),
    Column('reference_code', String(40), nullable=False),
    Column('bank_code', String(10), nullable=False),
    Column('branch_code', String(10), nullable=False),
    Column('account_number', String(40), nullable=False),
)

beneficiary_id = Column('beneficiary_id', Integer,
                        ForeignKey(beneficiary.c.id, name='invoice_beneficiary_id_fk'), nullable=False)

invoice = Table(
    'invoice', meta,
    beneficiary_id,
)


def upgrade(migrate_engine):
    meta.bind = migrate_engine
    beneficiary.create()
    beneficiary_id.create(invoice)


def downgrade(migrate_engine):
    meta.bind = migrate_engine
    ForeignKeyConstraint(table=invoice, name='invoice_beneficiary_id_fk',
                         columns=[invoice.c.beneficiary_id], refcolumns=[beneficiary.c.id]).drop()
    beneficiary_id.drop(invoice)
    beneficiary.drop()
