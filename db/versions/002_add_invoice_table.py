from sqlalchemy import *

meta = MetaData()

customer = Table(
    'customer', meta,
    Column('id', Integer, primary_key=True),
)

invoice = Table(
    'invoice', meta,
    Column('id', Integer, primary_key=True),
    Column('customer_id', Integer, ForeignKey(customer.c.id, name='invoice_customer_id_fk'), nullable=False),
    Column('amount', Numeric, nullable=False),
    Column('reference_code', String(40), nullable=False),
    Column('date', Date, nullable=False),
    Column('closing_date', Date, nullable=False),
    Column('due_date', Date, nullable=False),
    Column('paid_date', Date),
)


def upgrade(migrate_engine):
    meta.bind = migrate_engine
    invoice.create()


def downgrade(migrate_engine):
    meta.bind = migrate_engine
    invoice.drop()
