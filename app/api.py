from flask_restless import APIManager


class API(object):
    def __init__(self, app=None, db=None):
        self.manager = APIManager(app, flask_sqlalchemy_db=db)

    def create(self):
        from models.customer import Customer
        from models.invoice import Invoice
        from models.beneficiary import Beneficiary
        from models.remitter import Remitter

        self.manager.create_api(Customer, methods=['GET', 'POST', 'PUT', 'DELETE'])
        self.manager.create_api(Invoice, methods=['GET', 'POST', 'PUT', 'DELETE'])
        self.manager.create_api(Beneficiary, methods=['GET', 'POST', 'PUT', 'DELETE'])
        self.manager.create_api(Remitter, methods=['GET', 'POST', 'PUT', 'DELETE'])
