from datetime import date

from billing import db


class Invoice(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    customer_id = db.Column(db.Integer, db.ForeignKey('customer.id'), nullable=False)
    customer = db.relationship('Customer', foreign_keys=[customer_id],
                               backref=db.backref('invoices', lazy=True))

    beneficiary_id = db.Column(db.Integer, db.ForeignKey('beneficiary.id'), nullable=False)
    beneficiary = db.relationship('Beneficiary', foreign_keys=[beneficiary_id],
                                  backref=db.backref('invoices', lazy=True))

    remitter_id = db.Column(db.Integer, db.ForeignKey('remitter.id'), nullable=False)
    remitter = db.relationship('Remitter', foreign_keys=[remitter_id],
                               backref=db.backref('invoices', lazy=True))

    amount = db.Column(db.Numeric, nullable=False)
    reference_code = db.Column(db.String(40), nullable=False)

    date = db.Column(db.Date, nullable=False)
    closing_date = db.Column(db.Date, nullable=False)
    due_date = db.Column(db.Date, nullable=False)
    paid_date = db.Column(db.Date)

    def is_paid(self):
        return self.paid_date is not None

    def is_overdue(self):
        return not self.is_paid() and self.due_date < date.today()
