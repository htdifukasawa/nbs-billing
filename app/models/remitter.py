from billing import db


class Remitter(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(255), nullable=False)
    reference_code = db.Column(db.String(40), nullable=False)
    bank_code = db.Column(db.String(10), nullable=False)
    branch_code = db.Column(db.String(10), nullable=False)
    account_number = db.Column(db.String(40), nullable=False)
