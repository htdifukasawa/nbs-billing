from billing import db


class Customer(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    code = db.Column(db.String(40), nullable=False, unique=True)
