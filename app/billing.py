import os

from flask import Flask, make_response
from flask_sqlalchemy import SQLAlchemy

from api import API

app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('DATABASE_URI', 'mysql:///billing_' + app.env)
db = SQLAlchemy(app)
api = API(app, db)


@app.route('/')
def index():
    return ''


@app.route('/smbc/beneficiary/<int:beneficiary_id>/<int:year>/<int:month>')
def beneficiary_smbc(beneficiary_id, year, month):
    from models.beneficiary import Beneficiary
    beneficiary = db.session.query(Beneficiary).get(beneficiary_id)
    from smbc import export_string
    response = make_response(export_string(beneficiary, year, month))
    response.mimetype = 'text/plain'
    return response


api.create()

if __name__ == '__main__':
    app.run()
