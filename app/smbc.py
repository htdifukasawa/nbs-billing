import io
from datetime import date
from dateutil.relativedelta import relativedelta

from models.invoice import Invoice
from billing import db


def export_file(filename, invoice):
    with open(filename, 'w') as out:
        __invoice(out, invoice)


def export_string(beneficiary, year, month):
    out = io.StringIO()
    __invoices(out, beneficiary, year, month)
    return out.getvalue()


def __invoices(out, beneficiary, year, month):
    start_date = date(year, month, 1)
    end_date = start_date + relativedelta(months=1)

    __header_record(out, start_date, beneficiary)

    invoices = db.session.query(Invoice) \
        .filter(Invoice.beneficiary == beneficiary) \
        .filter(Invoice.date >= start_date) \
        .filter(Invoice.date < end_date) \
        .all()

    total_amount = 0
    for i in invoices:
        __data_record(out, i)
        total_amount += i.amount

    __trailer_record(out, total_amount)
    __end_record(out)


def __invoice(out, invoice):
    __header_record(out, date.today(), invoice.beneficiary)
    __data_record(out, invoice)
    __trailer_record(out, invoice.amount)
    __end_record(out)


def __header_record(out, date, beneficiary):
    out.write('1')
    out.write('91')
    out.write('1')
    out.write(beneficiary.reference_code.rjust(10, '0'))
    out.write(__hankaku(beneficiary.name).ljust(40))
    out.write(date.strftime('%y%m'))
    out.write(beneficiary.bank_code.rjust(4, '0'))
    out.write(__filler(15))
    out.write(beneficiary.branch_code.rjust(3, '0'))
    out.write(__filler(15))
    out.write('2')
    out.write(beneficiary.account_number.rjust(7, '0'))
    out.write(__filler(17))


def __data_record(out, invoice):
    out.write('2')
    out.write(invoice.remitter.bank_code.rjust(4, '0'))
    out.write(__filler(15))
    out.write(invoice.remitter.branch_code.rjust(3, '0'))
    out.write(__filler(15))
    out.write(__filler(4))
    out.write('1')
    out.write(invoice.remitter.account_number.rjust(7, '0'))
    out.write(__hankaku(invoice.remitter.name).ljust(30))
    out.write(str(invoice.amount).rjust(10, '0'))
    out.write('0')
    out.write(invoice.beneficiary.reference_code.rjust(8, '0'))
    out.write('00')
    out.write(invoice.reference_code.rjust(10, '0'))
    out.write('0')
    out.write(invoice.due_date.strftime('%y%m%d'))
    out.write(__filler(2))


def __trailer_record(out, total_amount):
    out.write('8')
    out.write(str(5).rjust(6, '0'))
    out.write(str(total_amount).rjust(12, '0'))
    out.write(__filler(101))


def __end_record(out):
    out.write('9')
    out.write(__filler(119))


def __filler(length):
    return ''.ljust(length)


def __hankaku(string):
    return string
