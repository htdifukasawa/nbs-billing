from datetime import datetime

import cx_Oracle
import requests
import json

API = 'http://127.0.0.1:5000/api'


def __connect():
    return cx_Oracle.connect(
        "TESTFRAMGIA",
        "NICHIGAS2017TESTFRAMGIA",
        "test-framgia-db.crr28czh8iiu.ap-northeast-1.rds.amazonaws.com/FRAMGIA",
        encoding='utf8'
    )


def __select_invoices(connection):
    cursor = connection.cursor()
    cursor.execute("""
SELECT S.ZEIKOMI_SEIKYU_GAKU_SUM                     AS "amount",
       S.SEIKYU_CODE                                 AS "reference_code",
       TO_DATE(S.SEIKYU_DATE, 'YYYYMMDD')            AS "date",
       TO_DATE(S.SEIKYUSHO_SAKUSEI_DATE, 'YYYYMMDD') AS "closing_date",
       TO_NUMBER(C.FURIKAE_KIJUN_DATE)               AS "payment_day",
       TO_NUMBER(K.FURIKAE_KIJUN_DATE)               AS "default_payment_day",
       S.KAISHA_CODE                                 AS "customer_code",

       K.ITAKUSHA_NAME                               AS "beneficiary_name",
       K.ITAKUSHA_CODE                               AS "beneficiary_reference_code",
       K.KOZAFURIKAE_KIKAN_BANK_CODE                 AS "beneficiary_bank_code",
       K.KOZAFURIKAE_KIKAN_SHITEN_CODE               AS "beneficiary_branch_code",
       K.KOZA_NO                                     AS "beneficiary_account_number",

       A.NAME                                        AS "remitter_name",
       A.CUST_CODE                                   AS "remitter_reference_code",
       C.KOZAFURIKAE_KIKAN_BANK_CODE                 AS "remitter_bank_code",
       C.KOZAFURIKAE_KIKAN_SHITEN_CODE               AS "remitter_branch_code",
       C.KOZA_NO                                     AS "remitter_account_number"

FROM SNK_T_SEIKYU S
       JOIN COM_M_CUST_SEIKYU_HOHO C
            ON (
                C.KAISHA_CODE = S.KAISHA_CODE
                AND C.CUST_CODE = S.SEIKYUSAKI_CODE
                AND C.START_YM <= SUBSTR(S.SEIKYU_DATE, 0, 6)
                AND ((C.END_YM IS NULL) OR (C.END_YM >= SUBSTR(S.SEIKYU_DATE, 0, 6)))
              )
       JOIN SNK_M_KOZA_FURI_KIKAN K
            ON (
                K.KAISHA_CODE = S.KAISHA_CODE
                AND K.KOZAFURIKAE_KIKAN_BANK_CODE = S.BANK_CODE
                AND K.KOZAFURIKAE_KIKAN_SHITEN_CODE = S.TEMPO_CODE
                AND K.FURIKAE_IRAI_YM = SUBSTR(S.SEIKYU_DATE, 0, 6)
              )
       JOIN COM_M_CUST_ADDRESS A
            ON (
                A.KAISHA_CODE = S.KAISHA_CODE
                AND A.CUST_CODE = S.SEIKYUSAKI_CODE
              )

WHERE S.ZEIKOMI_SEIKYU_GAKU_SUM IS NOT NULL
  AND S.SEIKYU_CODE IS NOT NULL
  AND S.DELETE_FLG = '0'
  
  AND S.SEIKYU_DATE >= '201811'
    """)
    return cursor


def __select_business_day(connection, date, customer_code):
    cursor = connection.cursor()
    cursor.execute("""
SELECT TO_DATE(MIN(YMD), 'YYYYMMDD') AS "business_day"
FROM COM_M_EIGYOBI
WHERE KAISHA_CODE = :customer_code
  AND YMD > :ymd
  AND EIGYOBI_FLG = '0'
    """, ymd=date.strftime('%Y%m%d'), customer_code=customer_code)
    return cursor.fetchone()[0]


def __iso_date(to_date):
    return to_date.date().isoformat()


def __add_to_dict_and_return(dictionary, key, value):
    dictionary[key] = value
    return value


def __create_customer(customer_code):
    customer = {
        "code": customer_code
    }
    response = requests.post(API + '/customer', json=customer)
    response.raise_for_status()
    return response.json()['id']


customer_ids_by_code = dict()


def __find_customer_id(customer_code):
    if customer_code in customer_ids_by_code:
        return customer_ids_by_code[customer_code]

    url = API + '/customer'
    headers = {'Content-Type': 'application/json'}

    filters = [dict(name='code', op='eq', val=customer_code)]
    params = dict(q=json.dumps(dict(filters=filters, single=True)))

    response = requests.get(url, params=params, headers=headers)
    if response.status_code == requests.codes.ok:
        return __add_to_dict_and_return(
            customer_ids_by_code,
            customer_code,
            response.json()['id'])
    else:
        return __add_to_dict_and_return(
            customer_ids_by_code,
            customer_code,
            __create_customer(customer_code))


def __create_party(type, name, reference_code, bank_code, branch_code, account_number):
    party = {
        "name": name,
        "reference_code": reference_code,
        "bank_code": bank_code,
        "branch_code": branch_code,
        "account_number": account_number
    }
    response = requests.post(API + '/' + type, json=party)
    response.raise_for_status()
    return response.json()['id']


party_ids_by_code = dict(
    beneficiary=dict(),
    remitter=dict()
)


def __find_party_id(type, name, reference_code, bank_code, branch_code, account_number):
    party_code = '/'.join([reference_code, bank_code, branch_code, account_number])
    if party_code in party_ids_by_code[type]:
        return party_ids_by_code[type][party_code]

    url = API + '/' + type
    headers = {'Content-Type': 'application/json'}

    filters = [
        dict(name='reference_code', op='eq', val=reference_code),
        dict(name='bank_code', op='eq', val=bank_code),
        dict(name='branch_code', op='eq', val=branch_code),
        dict(name='account_number', op='eq', val=account_number)
    ]
    params = dict(q=json.dumps(dict(filters=filters, single=True)))

    response = requests.get(url, params=params, headers=headers)
    if response.status_code == requests.codes.ok:
        return __add_to_dict_and_return(
            party_ids_by_code[type],
            party_code,
            response.json()['id'])
    else:
        return __add_to_dict_and_return(
            party_ids_by_code[type],
            party_code,
            __create_party(type, name, reference_code, bank_code, branch_code, account_number))


def __find_beneficiary_id(name, reference_code, bank_code, branch_code, account_number):
    return __find_party_id('beneficiary', name, reference_code, bank_code, branch_code, account_number)


def __find_remitter_id(name, reference_code, bank_code, branch_code, account_number):
    return __find_party_id('remitter', name, reference_code, bank_code, branch_code, account_number)


business_days = dict()


def __business_day(date, customer_code, connection):
    if customer_code not in business_days:
        business_days[customer_code] = dict()

    if date in business_days[customer_code]:
        return business_days[customer_code][date]

    return __add_to_dict_and_return(
        business_days[customer_code], date, __select_business_day(connection, date, customer_code))


def __calculate_due_date(date, payment_day, default_payment_day, customer_code, connection):
    return __business_day(datetime(date.year, date.month + 1, payment_day or default_payment_day),
                          customer_code, connection)


def __insert_invoices(cursor, connection):
    for amount, \
        reference_code, \
        date, \
        closing_date, \
        payment_day, \
        default_payment_day, \
        customer_code, \
 \
        beneficiary_name, \
        beneficiary_reference_code, \
        beneficiary_bank_code, \
        beneficiary_branch_code, \
        beneficiary_account_number, \
 \
        remitter_name, \
        remitter_reference_code, \
        remitter_bank_code, \
        remitter_branch_code, \
        remitter_account_number \
 \
            in cursor:
        due_date = __calculate_due_date(date, payment_day, default_payment_day, customer_code, connection)
        invoice = {
            "amount": amount,
            "reference_code": reference_code,
            "date": __iso_date(date),
            "closing_date": __iso_date(closing_date or date),
            "due_date": __iso_date(due_date),
            "customer_id": __find_customer_id(customer_code),
            "beneficiary_id": __find_beneficiary_id(
                beneficiary_name,
                beneficiary_reference_code,
                beneficiary_bank_code,
                beneficiary_branch_code,
                beneficiary_account_number
            ),
            "remitter_id": __find_remitter_id(
                remitter_name,
                remitter_reference_code,
                remitter_bank_code,
                remitter_branch_code,
                remitter_account_number
            )
        }
        response = requests.post(API + '/invoice', json=invoice)
        response.raise_for_status()
        print('.', end='', flush=True)


def __import_invoices(connection):
    __insert_invoices(__select_invoices(connection), connection)


__import_invoices(__connect())
