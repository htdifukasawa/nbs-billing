# billing
Nichigas: New Billing System

# database
The directory `db` contains our database migration repository.
See http://code.google.com/p/sqlalchemy-migrate/ for more information.

# server

## setup
```sh
pipenv install --dev
```

## tests
```sh
PYTHONPATH=app pipenv run pytest
```

## run
```sh
FLASK_APP=app/billing.py pipenv run flask run
```

# ui

## setup
```sh
npm install
```

## tests
```sh
npm test
```

## run
```
npm start
```
