from smbc import export_file
from smbc import export_string


def test_export_file(tmpdir, invoice):
    file = tmpdir.join('test.txt')
    export_file(file.strpath, invoice)
    content = file.read().encode('SHIFT_JIS')
    assert content[:23] == b'19110000195000\xc6\xce\xdd\xb6\xde\xbd(\xb6 '


def test_export_string(invoice):
    content = export_string(invoice).encode('SHIFT_JIS')
    assert content[:23] == b'19110000195000\xc6\xce\xdd\xb6\xde\xbd(\xb6 '
