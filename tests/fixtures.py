from datetime import date

import pytest

from models.invoice import Invoice
from models.beneficiary import Beneficiary
from models.remitter import Remitter


@pytest.fixture
def beneficiary():
    result = Beneficiary()
    result.name = 'ﾆﾎﾝｶﾞｽ(ｶ'
    result.reference_code = '195000'
    result.bank_code = '106'
    result.branch_code = '9'
    result.account_number = '0415178'
    return result


@pytest.fixture
def remitter():
    result = Remitter()
    result.name = 'ﾆﾁｶﾞｽ ﾀﾛｳ'
    result.reference_code = '195000'
    result.bank_code = '9'
    result.branch_code = '123'
    result.account_number = '1234567'
    return result


@pytest.fixture
def invoice(remitter, beneficiary):
    result = Invoice()
    result.amount = 1880
    result.reference_code = '1808633463'
    result.date = date(2019, 1, 1)
    result.closing_date = date(2018, 12, 31)
    result.due_date = date(2019, 1, 15)
    result.beneficiary = beneficiary
    result.remitter = remitter
    return result
