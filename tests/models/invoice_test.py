from datetime import date, timedelta


def test_is_not_paid(invoice):
    invoice.paid_date = None
    assert not invoice.is_paid()


def test_is_paid(invoice):
    invoice.paid_date = date(2019, 1, 1)
    assert invoice.is_paid()


def test_is_not_overdue(invoice):
    invoice.due_date = date.today()
    assert not invoice.is_overdue()


def test_is_overdue(invoice):
    invoice.due_date = date.today() - timedelta(days=1)
    assert invoice.is_overdue()


def test_is_not_overdue_if_is_paid(invoice):
    invoice.due_date = date.today() - timedelta(days=1)
    invoice.paid_date = date(2019, 1, 1)
    assert not invoice.is_overdue()
