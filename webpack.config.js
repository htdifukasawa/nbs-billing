const HtmlWebPackPlugin = require("html-webpack-plugin")

module.exports = {
  devServer: {
    proxy: {
      '/api': 'http://localhost:5000',
      '/smbc': 'http://localhost:5000',
    }
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader"
          }
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: "./src/index.html",
      filename: "./index.html"
    })
  ]
}
